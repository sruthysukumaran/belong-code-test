package com.telecom.phonenumber;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.telecom.phonenumber.controller.PhoneNumberController;
import com.telecom.phonenumber.model.PhoneNumber;
import com.telecom.phonenumber.service.PhoneService;

@RunWith(MockitoJUnitRunner.class)
public class PhoneControllerTest {
    @Mock
    private PhoneService phoneService;

    @InjectMocks
    private PhoneNumberController phoneController;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(phoneController).build();
    }

    @Test
    public void testGetAllPhones() throws Exception {
        List<PhoneNumber> phones = new ArrayList<>();
        phones.add(new PhoneNumber(1L, "1234567890", false, null));
        phones.add(new PhoneNumber(2L, "2345678901", true, null));
        Mockito.when(phoneService.getAllPhones()).thenReturn(phones);

        mockMvc.perform(get("/phone-numbers")
        .content(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk());
        
    }

    @Test
    public void testGetPhonesByCustomerId() throws Exception {
 
        List<PhoneNumber> phones = new ArrayList<>();
        phones.add(new PhoneNumber(1L, "1234567890", false, null));
        phones.add(new PhoneNumber(2L, "2345678901", true, null));
        Mockito.when(phoneService.getPhonesByCustomerId(1L)).thenReturn(phones);

        mockMvc.perform(get("/phone-numbers/1"))
            .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testActivatePhone() throws Exception {
        Mockito.doNothing().when(phoneService).activatePhone(1L);

        mockMvc.perform(MockMvcRequestBuilders.put("/phone-numbers/1/activate"))
            .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
