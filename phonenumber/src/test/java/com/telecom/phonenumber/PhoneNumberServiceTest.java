package com.telecom.phonenumber;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.telecom.phonenumber.exception.PhoneNumberNotFoundException;
import com.telecom.phonenumber.model.Customer;
import com.telecom.phonenumber.model.PhoneNumber;
import com.telecom.phonenumber.repository.CustomerRepository;
import com.telecom.phonenumber.repository.PhoneNumberRepository;
import com.telecom.phonenumber.service.PhoneService;

@ExtendWith(MockitoExtension.class)
public class PhoneNumberServiceTest {

	@Mock
	private PhoneNumberRepository phoneRepository;

	@Mock
	private CustomerRepository customerRepository;

	@InjectMocks
	private PhoneService phoneService;

	@Test
	public void testGetAllPhones() {

		List<PhoneNumber> testPhoneNumbers = Arrays.asList(new PhoneNumber(), new PhoneNumber(), new PhoneNumber());
		when(phoneRepository.findAll()).thenReturn(testPhoneNumbers);

		List<PhoneNumber> resultPhoneNumbers = phoneService.getAllPhones();
		Assertions.assertEquals(testPhoneNumbers, resultPhoneNumbers);
	}

	@Test
	public void testGetPhonesByCustomerId() {
		Long testCustomerId = 1L;

		when(customerRepository.findById(testCustomerId)).thenReturn(Optional.of(new Customer()));

		List<PhoneNumber> testPhoneList = new ArrayList<>();
		testPhoneList.add(new PhoneNumber());
		when(phoneRepository.findByCustomerId(testCustomerId)).thenReturn(testPhoneList);

		List<PhoneNumber> resultPhoneList = phoneService.getPhonesByCustomerId(testCustomerId);
		Assertions.assertEquals(testPhoneList, resultPhoneList);
	}

	@Test
	public void testActivatePhone() throws PhoneNumberNotFoundException {
		Long testPhoneNumberId = 1L;

		when(phoneRepository.findById(testPhoneNumberId)).thenReturn(Optional.of(new PhoneNumber()));

		phoneService.activatePhone(testPhoneNumberId);
		Optional<PhoneNumber> resultPhone = phoneRepository.findById(testPhoneNumberId);
		Assertions.assertTrue(resultPhone.isPresent());
		Assertions.assertTrue(resultPhone.get().isActivated());
	}

    @Test
    public void testActivatePhoneWhenPhoneNumberNotFound() {
        Long phoneNumberId = 1L;
        when(phoneRepository.findById(phoneNumberId)).thenReturn(Optional.empty());
       
        assertThrows(PhoneNumberNotFoundException.class,
                () ->  phoneService.activatePhone(phoneNumberId));
    }
}
