package com.telecom.phonenumber.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telecom.phonenumber.exception.PhoneNumberNotFoundException;
import com.telecom.phonenumber.model.PhoneNumber;
import com.telecom.phonenumber.service.PhoneService;

@RestController
public class PhoneNumberController {
    @Autowired
    private PhoneService phoneService;

    @GetMapping("/phone-numbers")
    public List<PhoneNumber> getAllPhones() {
        return phoneService.getAllPhones();
    }

    @GetMapping("/phone-numbers/{customerId}")
    public List<PhoneNumber> getPhonesByCustomerId(@PathVariable Long customerId) {
        return phoneService.getPhonesByCustomerId(customerId);
    }

    @PutMapping("/phone-numbers/{phoneId}/activate")
    public void activatePhone(@PathVariable Long phoneId) throws PhoneNumberNotFoundException {
        phoneService.activatePhone(phoneId);
    }
}