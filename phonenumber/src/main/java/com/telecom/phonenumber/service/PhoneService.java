package com.telecom.phonenumber.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.telecom.phonenumber.exception.PhoneNumberNotFoundException;
import com.telecom.phonenumber.exception.ResourceNotFoundException;
import com.telecom.phonenumber.model.PhoneNumber;
import com.telecom.phonenumber.repository.CustomerRepository;
import com.telecom.phonenumber.repository.PhoneNumberRepository;

@Service
@Component
public class PhoneService {
	
    @Autowired
    private PhoneNumberRepository phoneRepository;
    @Autowired
    private CustomerRepository customerRepository;

    public List<PhoneNumber> getAllPhones() {
        return phoneRepository.findAll();
    }

    public List<PhoneNumber> getPhonesByCustomerId(Long customerId) {
    	customerRepository.findById(customerId)
                 .orElseThrow(() -> new ResourceNotFoundException("Customer", "id", customerId));
        return phoneRepository.findByCustomerId(customerId);
    }

    public void activatePhone(Long phoneNumberId) throws PhoneNumberNotFoundException {
        Optional<PhoneNumber> phone = phoneRepository.findById(phoneNumberId);
        if (phone.isPresent()) {
        	PhoneNumber p = phone.get();
            p.setActivated(true);
            phoneRepository.save(p);
        } else {
        	 throw new PhoneNumberNotFoundException("Phone number not found with id: %s" , phoneNumberId);
        }
    }
}