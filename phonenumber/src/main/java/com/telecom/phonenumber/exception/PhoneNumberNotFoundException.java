package com.telecom.phonenumber.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@SuppressWarnings("unused")
public class PhoneNumberNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;
	
	private String msg;
    private Long fieldValue;

    public PhoneNumberNotFoundException( String msg, Long fieldValue) {
        this.msg = msg;
        this.fieldValue = fieldValue;
    }
}
