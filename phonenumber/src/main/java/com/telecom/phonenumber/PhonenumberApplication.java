package com.telecom.phonenumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = {"com.telecom.phonenumber.model"})
@EnableJpaRepositories(basePackages ="com.telecom.phonenumber.repository")
public class PhonenumberApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhonenumberApplication.class, args);
	}

}
