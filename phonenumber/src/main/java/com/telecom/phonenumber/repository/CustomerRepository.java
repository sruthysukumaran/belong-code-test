package com.telecom.phonenumber.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telecom.phonenumber.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>  {
}
