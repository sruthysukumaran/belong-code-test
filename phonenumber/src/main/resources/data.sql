CREATE SCHEMA my_schema;

CREATE TABLE my_schema.customer (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL
);

CREATE TABLE my_schema.phonenumber (
  id INT PRIMARY KEY AUTO_INCREMENT,
  phone_number VARCHAR(255) NOT NULL,
  is_active BOOLEAN DEFAULT false,
  customer_id INT,
  FOREIGN KEY (customer_id) REFERENCES my_schema.customer(id)
);


